import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todolist_bloc_project/page/home_page.dart';

import 'bloc/todolist/todolist_bloc.dart';
import 'bloc/todolist/todolist_state.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => TodolistBloc(UninitializeTodoState()),
        child:
            MaterialApp(debugShowCheckedModeBanner: false, home: HomePage()));
  }
}
