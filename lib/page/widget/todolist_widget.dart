import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:todolist_bloc_project/bloc/todolist/todolist_bloc.dart';
import 'package:todolist_bloc_project/bloc/todolist/todolist_event.dart';
import 'package:todolist_bloc_project/page/edit_item_todolist_form.dart';

class TodolistWidget extends StatelessWidget {
  final _todo;
  final _taskIndex;

  TodolistWidget(this._todo, this._taskIndex);
  @override
  Widget build(BuildContext context) {
    Color todoState;
    switch (_todo.state) {
      case 0:
        todoState = Colors.red;
        break;
      case 1:
        todoState = Colors.orange;
        break;
      case 2:
        todoState = Colors.green;
        break;
      default:
        todoState = Colors.red;
        break;
    }
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(2.0)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.4),
                      spreadRadius: 2,
                      blurRadius: 3,
                      offset: Offset(2, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20, vertical: 10.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            onTapUp: (_) {
                              BlocProvider.of<TodolistBloc>(context).add(
                                  UpdateTodoListStateEvent(
                                      _taskIndex, _todo.state));
                            },
                            child: Container(
                              height: 10.0,
                              width: 10.0,
                              decoration: new BoxDecoration(
                                color: todoState,
                                shape: BoxShape.circle,
                              ),
                            ),
                          ),
                          Container(
                            width: 35.0,
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _todo.taskName,
                                  style: GoogleFonts.lato(
                                      textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  )),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTapUp: (_) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          EditItemTodoListForm(
                                              _todo, _taskIndex)));
                            },
                            child: Icon(
                              Icons.edit,
                              color: Colors.blue,
                            ),
                          ),
                          Container(
                            width: 5.0,
                          ),
                          GestureDetector(
                            onTapUp: (_) {
                              BlocProvider.of<TodolistBloc>(context)
                                  .add(DeleteTodoListEvent(_taskIndex));
                            },
                            child: Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.insert_invitation,
                              color: Colors.grey,
                              size: 16.0,
                            ),
                            Container(
                              width: 3.0,
                            ),
                            Text(
                              _todo.date,
                              style: GoogleFonts.lato(
                                  textStyle: TextStyle(
                                color: Colors.grey,
                                fontSize: 12.0,
                              )),
                            ),
                            Container(
                              width: 30.0,
                            ),
                            Icon(
                              Icons.access_time,
                              color: Colors.grey,
                              size: 16.0,
                            ),
                            Container(
                              width: 3.0,
                            ),
                            Text(
                              _todo.time,
                              style: GoogleFonts.lato(
                                  textStyle: TextStyle(
                                color: Colors.grey,
                                fontSize: 12.0,
                              )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
