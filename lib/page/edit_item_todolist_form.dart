import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:todolist_bloc_project/bloc/todolist/todolist_bloc.dart';
import 'package:todolist_bloc_project/bloc/todolist/todolist_event.dart';
import 'package:todolist_bloc_project/model/todolist_model.dart';
import 'package:todolist_bloc_project/page/home_page.dart';

class EditItemTodoListForm extends StatefulWidget {
  TodoList _todo;
  int _element_index;

  EditItemTodoListForm(this._todo, this._element_index);

  @override
  _EditItemTodoListFormState createState() =>
      _EditItemTodoListFormState(_todo, _element_index);
}

class _EditItemTodoListFormState extends State<EditItemTodoListForm> {
  TodoList _todo;
  int _element_index;

  _EditItemTodoListFormState(this._todo, this._element_index);

  @override
  Widget build(BuildContext context) {
    TextEditingController _taskNameController =
        TextEditingController(text: _todo.taskName);
    return Container(
      color: Color(0xffeff0f1),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Change ${_todo.taskName}'),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          child: Form(
            child: Column(
              children: <Widget>[
                TextFormField(
                    decoration: InputDecoration(labelText: 'Task name'),
                    controller: _taskNameController),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.blueAccent, width: 1.0))),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(_todo.date + ' ' + _todo.time),
                        ),
                      ),
                    ),
                    Container(
                      width: 12.0,
                    ),
                    Expanded(
                      child: RaisedButton(
                          onPressed: () {
                            DatePicker.showDateTimePicker(context,
                                theme: DatePickerTheme(
                                  headerColor: Colors.blueAccent,
                                  itemStyle: TextStyle(
                                      color: Colors.black, fontSize: 18),
                                  doneStyle: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ), onConfirm: (date) {
                              var splittedDate = date.toString().split(' ');
                              var dateFr =
                                  '${splittedDate[0].split('-')[2]}/${splittedDate[0].split('-')[1]}/${splittedDate[0].split('-')[0]}';
                              var time =
                                  '${splittedDate[1].split(':')[0]}h${splittedDate[1].split(':')[1]}';
                              _todo.date = dateFr;
                              _todo.time = time;
                              setState(() => {});
                            },
                                currentTime: DateTime.now(),
                                locale: LocaleType.fr);
                          },
                          child: Text(
                            'choose date',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(7.0)),
                            side: BorderSide(color: Colors.blueAccent),
                          )),
                    ),
                  ],
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(7.0)),
                    side: BorderSide(color: Colors.green),
                  ),
                  child: Text(
                    'Send',
                    style: TextStyle(color: Colors.green),
                  ),
                  color: Colors.white,
                  onPressed: () {
                    if (_taskNameController.text != '' &&
                        _todo.date != '' &&
                        _todo.time != '') {
                      BlocProvider.of<TodolistBloc>(context).add(
                          UpdateTodoListEvent(
                              _element_index,
                              _taskNameController.text,
                              _todo.date,
                              _todo.time));
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => HomePage()));
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
