import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:todolist_bloc_project/bloc/todolist/todolist_bloc.dart';
import 'package:todolist_bloc_project/bloc/todolist/todolist_state.dart';
import 'package:todolist_bloc_project/page/add_item_todolist_form.dart';
import 'package:todolist_bloc_project/page/widget/todolist_widget.dart';

class HomePage extends StatelessWidget {
  final GlobalKey<AnimatedListState> _animatedListKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 8.0, right: 20.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Todo List',
                    style: GoogleFonts.galada(
                        textStyle: TextStyle(
                            fontSize: 33.0, fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            ),
            BlocBuilder<TodolistBloc, TodolistState>(
              builder: (context, state) {
                if (state is UninitializeTodoState) {
                  return Expanded(child: Text('Add an element'));
                } else if (state is HasItemsTodolistState) {
                  return Expanded(
                    child: ListView.builder(
                      itemCount: state.todolist.length,
                      itemBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 40.0, vertical: 10.0),
                        child: TodolistWidget(state.todolist[index], index),
                      ),
                    ),
                  );
                }
                return Text('Error');
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blueAccent,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () {
          print('pressed');
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => AddItemTodoListForm()));
        },
      ),
    );
  }
}
