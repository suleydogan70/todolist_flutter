class TodolistEvent {}

class AddTodoListEvent extends TodolistEvent {
  final String taskName;
  final String date;
  final String time;

  AddTodoListEvent(this.taskName, this.date, this.time);
}

class UpdateTodoListStateEvent extends TodolistEvent {
  final int element_index;
  final int state;

  UpdateTodoListStateEvent(this.element_index, this.state);
}

class UpdateTodoListEvent extends TodolistEvent {
  final int element_index;
  final String taskName;
  final String date;
  final String time;

  UpdateTodoListEvent(this.element_index, this.taskName, this.date, this.time);
}

class DeleteTodoListEvent extends TodolistEvent {
  final int element_index;

  DeleteTodoListEvent(this.element_index);
}

class ResetTodolistEvent extends TodolistEvent {}
