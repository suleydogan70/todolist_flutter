import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todolist_bloc_project/bloc/todolist/todolist_event.dart';
import 'package:todolist_bloc_project/bloc/todolist/todolist_state.dart';
import 'package:todolist_bloc_project/model/todolist_model.dart';
import 'package:todolist_bloc_project/repository/todolist_repository.dart';

class TodolistBloc extends Bloc<TodolistEvent, TodolistState> {
  TodolistBloc(TodolistState initialState) : super(initialState);

  @override
  Stream<TodolistState> mapEventToState(TodolistEvent event) async* {
    TodolistRepository todolistRepository = TodolistRepository();

    if (event is AddTodoListEvent) {
      final DateTime now = new DateTime.now();
      todolistRepository.todolist
          .add(TodoList(event.taskName, 0, event.date, event.time));
      yield HasItemsTodolistState(todolistRepository.todolist);
    } else if (event is DeleteTodoListEvent) {
      todolistRepository.todolist.removeAt(event.element_index);
      if (todolistRepository.todolist.length != 0) {
        yield HasItemsTodolistState(todolistRepository.todolist);
      } else {
        yield UninitializeTodoState();
      }
    } else if (event is UpdateTodoListEvent) {
      todolistRepository.todolist[event.element_index].taskName =
          event.taskName;
      todolistRepository.todolist[event.element_index].date = event.date;
      todolistRepository.todolist[event.element_index].time = event.time;
      yield HasItemsTodolistState(todolistRepository.todolist);
    } else if (event is UpdateTodoListStateEvent) {
      if (todolistRepository.todolist[event.element_index].state == 2) {
        todolistRepository.todolist[event.element_index].state = 0;
      } else {
        todolistRepository.todolist[event.element_index].state += 1;
      }
      yield HasItemsTodolistState(todolistRepository.todolist);
    } else if (event is ResetTodolistEvent) {
      yield UninitializeTodoState();
    }
  }
}
