class TodolistRepository {
  List todolist = [];

  static final TodolistRepository _todolistRepository =
      TodolistRepository._internal();

  factory TodolistRepository() {
    return _todolistRepository;
  }

  TodolistRepository._internal();
}
